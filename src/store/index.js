import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import groups from './modules/groups';
import users from './modules/users';
import members from './modules/members';
import snackbar from './modules/snackbar';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    groups,
    users,
    members,
    snackbar
  }
});
