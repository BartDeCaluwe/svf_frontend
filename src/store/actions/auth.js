export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_CALLBACK = 'AUTH_CALLBACK';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_RENEW = 'AUTH_RENEW';
export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
