import {
  GET_GROUPS_REQUEST,
  GET_GROUPS_ERROR,
  GET_GROUPS_SUCCESS,
  CREATE_GROUP_REQUEST,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_ERROR,
  UPDATE_MEMBERSHIP_REQUEST,
  UPDATE_MEMBERSHIP_SUCCESS,
  UPDATE_MEMBERSHIP_ERROR,
  DELETE_GROUP_ERROR,
  DELETE_GROUP_REQUEST,
  DELETE_GROUP_SUCCESS,
  ARCHIVE_GROUP_REQUEST,
  ARCHIVE_GROUP_SUCCESS,
  ARCHIVE_GROUP_ERROR
} from '../actions/groups';
import axios from 'axios';
import {
  SHOW_ERROR_SNACKBAR,
  SHOW_SUCCESS_SNACKBAR
} from '../actions/snackbar';

const state = {
  isLoading: false,
  status: null,
  error: null,
  groups: null
};
export default {
  namespaced: true,
  state,
  getters: {},
  actions: {
    [GET_GROUPS_REQUEST]: ({ commit, dispatch }) => {
      commit(GET_GROUPS_REQUEST);
      return axios({ url: 'svf-groups/', method: 'GET' })
        .then(resp => {
          commit(GET_GROUPS_SUCCESS, resp.data.results);
        })
        .catch(err => {
          console.log(err);
          commit(GET_GROUPS_ERROR, err);
        });
    },
    [CREATE_GROUP_REQUEST]: ({ commit, dispatch }, data) => {
      commit(CREATE_GROUP_REQUEST);
      return axios({ url: 'svf-groups/', method: 'POST', data })
        .then(resp => {
          commit(CREATE_GROUP_SUCCESS, resp.data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Group created!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(CREATE_GROUP_ERROR, err);
          commit(`snackbar/${SHOW_ERROR_SNACKBAR}`, 'Failed to create group!', {
            root: true
          });
          return err;
        });
    },
    [DELETE_GROUP_REQUEST]: ({ commit, dispatch }, data) => {
      commit(DELETE_GROUP_REQUEST);
      return axios({ url: `svf-groups/${data.id}/`, method: 'DELETE' })
        .then(resp => {
          commit(DELETE_GROUP_SUCCESS, data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Group deleted!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(DELETE_GROUP_ERROR, err);
          commit(`snackbar/${SHOW_ERROR_SNACKBAR}`, 'Failed to delte group!', {
            root: true
          });
          return err;
        });
    },
    [UPDATE_MEMBERSHIP_REQUEST]: ({ commit, dispatch }, data) => {
      commit(UPDATE_MEMBERSHIP_REQUEST);
      return axios({
        url: `memberships/${data.membership_id}/`,
        method: 'PUT',
        data
      })
        .then(resp => {
          commit(UPDATE_MEMBERSHIP_SUCCESS, resp.data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Membership updated!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(UPDATE_MEMBERSHIP_ERROR, err);
          commit(
            `snackbar/${SHOW_ERROR_SNACKBAR}`,
            'Failed to update membership!',
            {
              root: true
            }
          );
          return err;
        });
    },
    [ARCHIVE_GROUP_REQUEST]: ({ commit, dispatch }, data) => {
      commit(ARCHIVE_GROUP_REQUEST);
      return axios({
        url: `svf-groups/${data.id}/archive/`,
        method: 'GET',
        data
      })
        .then(resp => {
          commit(ARCHIVE_GROUP_SUCCESS, data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Group archived', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(ARCHIVE_GROUP_ERROR, err);
          commit(
            `snackbar/${SHOW_ERROR_SNACKBAR}`,
            'Failed to archive group!',
            {
              root: true
            }
          );
          return err;
        });
    }
  },
  mutations: {
    [GET_GROUPS_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [CREATE_GROUP_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [ARCHIVE_GROUP_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [DELETE_GROUP_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [UPDATE_MEMBERSHIP_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [GET_GROUPS_SUCCESS]: (state, groups) => {
      state.status = 'success';
      state.isLoading = false;
      state.groups = groups;
    },
    [CREATE_GROUP_SUCCESS]: (state, group) => {
      state.status = 'success';
      state.groups = state.groups ? [...state.groups, group] : [group];
      state.isLoading = false;
    },
    [DELETE_GROUP_SUCCESS]: (state, group) => {
      state.status = 'success';
      state.groups = state.groups
        ? state.groups.filter(g => g.id !== group.id)
        : [];
      state.isLoading = false;
    },
    [ARCHIVE_GROUP_SUCCESS]: (state, group) => {
      state.status = 'success';
      state.groups = state.groups
        ? state.groups.filter(g => g.id !== group.id)
        : [];
      state.isLoading = false;
    },
    [GET_GROUPS_ERROR]: state => {
      state.isLoading = false;
      state.status = 'error';
    },
    [CREATE_GROUP_ERROR]: state => {
      state.status = 'error';
      state.isLoading = false;
    },
    [ARCHIVE_GROUP_ERROR]: state => {
      state.status = 'error';
      state.isLoading = false;
    },
    [UPDATE_MEMBERSHIP_ERROR]: state => {
      state.status = 'error';
      state.isLoading = false;
    },
    [UPDATE_MEMBERSHIP_SUCCESS]: (state, member) => {
      state.status = 'success';
      const index = state.groups.findIndex(g => g.id === member.group_id);
      const memberIndex = state.groups[index].members.findIndex(
        m => m.id === member.id
      );
      const originalMember = state.groups[index].members[memberIndex];
      state.groups[index].members[memberIndex] = Object.assign(
        {},
        originalMember,
        member
      );
      state.isLoading = false;
    }
  }
};
