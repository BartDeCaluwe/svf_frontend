import {
  GET_MEMBERS_REQUEST,
  GET_MEMBERS_ERROR,
  GET_MEMBERS_SUCCESS,
  CREATE_MEMBER_REQUEST,
  CREATE_MEMBER_SUCCESS,
  CREATE_MEMBER_ERROR,
  UPDATE_MEMBER_REQUEST,
  UPDATE_MEMBER_SUCCESS,
  UPDATE_MEMBER_ERROR,
  DELETE_MEMBER_ERROR,
  DELETE_MEMBER_REQUEST,
  DELETE_MEMBER_SUCCESS
} from '../actions/members';
import axios from 'axios';
import {
  SHOW_SUCCESS_SNACKBAR,
  SHOW_ERROR_SNACKBAR
} from '../actions/snackbar';

const state = {
  isLoading: false,
  status: null,
  error: null,
  members: null
};
export default {
  namespaced: true,
  state,
  getters: {},
  actions: {
    [GET_MEMBERS_REQUEST]: ({ commit, dispatch }) => {
      commit(GET_MEMBERS_REQUEST);
      return axios({ url: 'members/', method: 'GET' })
        .then(resp => {
          commit(GET_MEMBERS_SUCCESS, resp.data.results);
        })
        .catch(err => {
          console.log(err);
          commit(GET_MEMBERS_ERROR, err);
        });
    },
    [CREATE_MEMBER_REQUEST]: ({ commit, dispatch }, data) => {
      commit(CREATE_MEMBER_REQUEST);
      return axios({ url: 'members/', method: 'POST', data })
        .then(resp => {
          commit(CREATE_MEMBER_SUCCESS, resp.data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Member created!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(CREATE_MEMBER_ERROR, err);
          commit(
            `snackbar/${SHOW_ERROR_SNACKBAR}`,
            'Failed to create member!',
            {
              root: true
            }
          );
          return err;
        });
    },
    [UPDATE_MEMBER_REQUEST]: ({ commit, dispatch }, data) => {
      commit(UPDATE_MEMBER_REQUEST);
      return axios({ url: `members/${data.id}/`, method: 'PUT', data })
        .then(resp => {
          commit(UPDATE_MEMBER_SUCCESS, resp.data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Member updated!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(UPDATE_MEMBER_ERROR, err);
          commit(
            `snackbar/${SHOW_ERROR_SNACKBAR}`,
            'Failed to update member!',
            {
              root: true
            }
          );
          return err;
        });
    },
    [DELETE_MEMBER_REQUEST]: ({ commit, dispatch }, data) => {
      commit(DELETE_MEMBER_REQUEST);
      return axios({ url: `members/${data.id}/`, method: 'DELETE' })
        .then(resp => {
          commit(DELETE_MEMBER_SUCCESS, data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'Member deleted!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(DELETE_MEMBER_ERROR, err);
          commit(
            `snackbar/${SHOW_ERROR_SNACKBAR}`,
            'Failed to delete member!',
            {
              root: true
            }
          );
          return err;
        });
    }
  },
  mutations: {
    [GET_MEMBERS_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [UPDATE_MEMBER_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [DELETE_MEMBER_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [CREATE_MEMBER_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [GET_MEMBERS_SUCCESS]: (state, members) => {
      state.status = 'success';
      state.isLoading = false;
      state.members = members;
    },
    [GET_MEMBERS_ERROR]: state => {
      state.isLoading = false;
      state.status = 'error';
    },
    [CREATE_MEMBER_SUCCESS]: (state, member) => {
      state.status = 'success';
      state.members = state.members ? [...state.members, member] : [member];
      state.isLoading = false;
    },
    [DELETE_MEMBER_SUCCESS]: (state, member) => {
      state.status = 'success';
      state.members = state.members
        ? state.members.filter(m => m.id !== member.id)
        : [];
      state.isLoading = false;
    },
    [UPDATE_MEMBER_SUCCESS]: (state, member) => {
      state.status = 'success';
      const index = state.members.findIndex(m => m.id === member.id);
      const originalMember = state.members[index];
      state[index] = Object.assign({}, originalMember, member);
      state.isLoading = false;
    },
    [CREATE_MEMBER_ERROR]: state => {
      state.isLoading = false;
      state.status = 'error';
    }
  }
};
