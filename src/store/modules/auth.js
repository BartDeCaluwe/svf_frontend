import {
  AUTH_REQUEST,
  AUTH_ERROR,
  AUTH_SUCCESS,
  AUTH_LOGOUT,
  AUTH_CALLBACK,
  AUTH_RENEW
} from '../actions/auth';
import auth0 from 'auth0-js';
import router from './../../router';
import axios from 'axios';

const auth = new auth0.WebAuth({
  domain: process.env.VUE_APP_AUTH0_DOMAIN,
  clientID: process.env.VUE_APP_AUTH0_CLIENT_ID,
  redirectUri: `${process.env.VUE_APP_BASE_URL}/callback`,
  responseType: 'token id_token',
  audience: process.env.VUE_APP_AUTH0_API_AUDIENCE,
  scope: 'openid'
});
const state = {
  accessToken: localStorage.getItem('accessToken') || '',
  idToken: '',
  expiresAt: '',
  userProfile: '',
  status: '',
  isAuthenticated: localStorage.getItem('loggedIn') || false
};

export default {
  state,
  getters: {
    isAuthenticated: state => !!state.token,
    status: state => state.status
  },
  actions: {
    [AUTH_REQUEST]: ({ commit, dispatch }) => {
      commit(AUTH_REQUEST);
      auth.authorize();
    },
    [AUTH_LOGOUT]: ({ commit, dispatch }) => {
      localStorage.removeItem('loggedIn');
      localStorage.removeItem('token');
      commit(AUTH_LOGOUT);
      // navigate to the home route
      router.replace('/');
    },
    [AUTH_RENEW]: ({ commit, dispatch }) => {
      return new Promise((resolve, reject) => {
        auth.checkSession({}, (err, authResult) => {
          if (authResult && authResult.accessToken && authResult.idToken) {
            commit(AUTH_SUCCESS, authResult);
            resolve(authResult.accessToken);
          } else if (err) {
            dispatch(AUTH_LOGOUT);
            reject();
          }
        });
      });
    },
    [AUTH_CALLBACK]: ({ commit, dispatch }) => {
      auth.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          commit(AUTH_SUCCESS, authResult);
        } else if (err) {
          commit(AUTH_ERROR, err);
        }
        router.replace('/');
      });
    }
  },
  mutations: {
    [AUTH_REQUEST]: state => {
      state.status = 'loading';
    },
    [AUTH_SUCCESS]: (state, authResult) => {
      state.accessToken = authResult.accessToken;
      state.idToken = authResult.idToken;
      state.expiresAt = authResult.expiresIn * 1000 + new Date().getTime();

      localStorage.setItem('accessToken', authResult.accessToken);
      localStorage.setItem('loggedIn', true);
      state.status = 'success';
      state.isAuthenticated = true;

      // set bearer token
      axios.defaults.headers.common['Authorization'] = `Bearer ${
        authResult.accessToken
      }`;
    },
    [AUTH_ERROR]: state => {
      state.status = 'error';
    },
    [AUTH_LOGOUT]: state => {
      // Clear access token and ID token from local storage
      state.accessToken = null;
      state.idToken = null;
      state.expiresAt = null;
      state.userProfile = null;

      state.isAuthenticated = false;
      // remove bearer token
      delete axios.defaults.headers.common['Authorization'];
    }
  }
};
