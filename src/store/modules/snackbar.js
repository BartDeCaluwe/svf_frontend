import {
  SHOW_SUCCESS_SNACKBAR,
  DISMISS_SNACKBAR,
  SHOW_ERROR_SNACKBAR
} from '../actions/snackbar';

const state = {
  snackbar: false,
  color: null,
  text: null,
  top: true,
  right: true,
  bottom: null,
  left: null,
  timeout: 6000
};
export default {
  namespaced: true,
  state,
  getters: {},
  actions: {
    [SHOW_SUCCESS_SNACKBAR]: ({ commit }, text) => {
      commit(SHOW_SUCCESS_SNACKBAR, text);
    },
    [SHOW_ERROR_SNACKBAR]: ({ commit }, text) => {
      commit(SHOW_ERROR_SNACKBAR, text);
    },
    [DISMISS_SNACKBAR]: ({ commit }) => {
      commit(DISMISS_SNACKBAR);
    }
  },
  mutations: {
    [SHOW_SUCCESS_SNACKBAR]: (state, text) => {
      state.color = 'success';
      state.text = text;
      state.top = true;
      state.right = true;
      state.snackbar = true;
    },
    [SHOW_ERROR_SNACKBAR]: (state, text) => {
      state.color = 'error';
      state.text = text;
      state.top = true;
      state.bottom = false;
      state.right = false;
      state.snackbar = true;
    },
    [DISMISS_SNACKBAR]: (state, text) => {
      state.snackbar = false;
    }
  }
};
