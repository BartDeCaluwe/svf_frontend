import {
  GET_USERS_REQUEST,
  GET_USERS_ERROR,
  GET_USERS_SUCCESS,
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_ERROR
} from '../actions/users';
import axios from 'axios';
import { SHOW_SUCCESS_SNACKBAR } from '../actions/snackbar';

const state = {
  isLoading: false,
  status: null,
  error: null,
  users: null
};
export default {
  namespaced: true,
  state,
  getters: {},
  actions: {
    [GET_USERS_REQUEST]: ({ commit, dispatch }) => {
      commit(GET_USERS_REQUEST);
      return axios({ url: 'users/', method: 'GET' })
        .then(resp => {
          commit(GET_USERS_SUCCESS, resp.data);
        })
        .catch(err => {
          console.log(err);
          commit(GET_USERS_ERROR, err);
        });
    },
    [CREATE_USER_REQUEST]: ({ commit, dispatch }, data) => {
      commit(CREATE_USER_REQUEST);
      return axios({ url: 'users', method: 'POST', data })
        .then(resp => {
          commit(CREATE_USER_SUCCESS, resp.data);
          commit(`snackbar/${SHOW_SUCCESS_SNACKBAR}`, 'User created!', {
            root: true
          });
          return resp;
        })
        .catch(err => {
          console.log(err);
          commit(CREATE_USER_ERROR, err);
          return err;
        });
    }
  },
  mutations: {
    [GET_USERS_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [GET_USERS_SUCCESS]: (state, users) => {
      state.status = 'success';
      state.isLoading = false;
      state.users = users;
    },
    [GET_USERS_ERROR]: state => {
      state.isLoading = false;
      state.status = 'error';
    },
    [CREATE_USER_REQUEST]: state => {
      state.status = 'loading';
      state.isLoading = true;
    },
    [CREATE_USER_SUCCESS]: (state, user) => {
      state.status = 'success';
      state.users = state.users ? [...state.users, user] : [];
    },
    [CREATE_USER_ERROR]: state => {
      state.status = 'error';
    }
  }
};
