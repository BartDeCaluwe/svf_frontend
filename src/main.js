import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import Vuetify from 'vuetify';
import axios from 'axios';
import { AUTH_RENEW } from './store/actions/auth';
import 'vuetify/dist/vuetify.min.css';

// Auto authenticate the user if token is still present
const token = localStorage.getItem('accessToken');
if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}
axios.defaults.baseURL = process.env.VUE_APP_API_URL;

axios.interceptors.response.use(undefined, async function(err) {
  if (
    err.response.status === 401 &&
    err.config &&
    !err.config.__isRetryRequest
  ) {
    const token = await store.dispatch(AUTH_RENEW);
    console.log(token);
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    err.config.__isRetryRequest = true;
    return axios.request(err.config);
  }
  throw err;
});

Vue.config.productionTip = false;
Vue.use(Vuetify);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
