import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import { AUTH_REQUEST } from './store/actions/auth';
Vue.use(Router);

function requireAuth(to, from, next) {
  if (!store.state.auth.isAuthenticated) {
    store.dispatch(`${AUTH_REQUEST}`);
  } else {
    next();
  }
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: 'groups'
    },
    {
      path: '/callback',
      name: 'callback',
      component: () =>
        import(/* webpackChunkName: "callback" */ './views/Callback.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/groups',
      name: 'groups',
      beforeEnter: requireAuth,
      meta: {
        breadcrumb: [
          {
            text: 'Groups',
            to: 'groups'
          }
        ]
      },
      component: () =>
        import(/* webpackChunkName: "groups" */ './views/Groups.vue')
    },
    {
      path: '/groups/new',
      name: 'newGroup',
      beforeEnter: requireAuth,
      meta: {
        breadcrumb: [
          {
            text: 'Groups',
            to: '/groups',
            exact: true
          },
          {
            text: 'New group'
          }
        ]
      },
      component: () =>
        import(/* webpackChunkName: "newGroup" */ './views/CreateGroup.vue')
    },
    {
      path: '/members',
      name: 'members',
      beforeEnter: requireAuth,
      meta: {
        breadcrumb: [
          {
            text: 'Members',
            to: 'members'
          }
        ]
      },
      component: () =>
        import(/* webpackChunkName: "members" */ './views/Members.vue')
    },
    {
      path: '/members/new',
      name: 'newMember',
      beforeEnter: requireAuth,
      meta: {
        breadcrumb: [
          {
            text: 'Members',
            to: '/members',
            exact: true
          },
          {
            text: 'New member'
          }
        ]
      },
      component: () =>
        import(/* webpackChunkName: "newMember" */ './views/CreateMember.vue')
    }
  ]
});
